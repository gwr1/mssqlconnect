#####
This is simple wrapper for create mssql engine based on sqlalchemy
#####

Usage:

from mssqlconnect import engine
import pandas

sql_query = 'select 1'
df = pandas.read_sql(sql_query, engine)

