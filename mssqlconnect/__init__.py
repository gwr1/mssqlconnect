import sqlalchemy
import os

os.environ['TDSVER'] = '8.0'

username = os.environ['MSSQL_ODBC_USERNAME']
password = os.environ['MSSQL_ODBC_PASSWORD']
server = host = os.environ['MSSQL_ODBC_HOST']
port = os.environ['MSSQL_ODBC_PORT']
hostport = '%s:%s' % (host, port)
database = os.environ['MSSQL_ODBC_DATABASE']
driver = os.environ['MSSQL_ODBC_DRIVER']

connection_string = "mssql+pyodbc://%s:%s@%s/%s?driver=%s" % (username, password, hostport, database, driver)

engine = sqlalchemy.create_engine(connection_string)
#connection = engine.connect()


